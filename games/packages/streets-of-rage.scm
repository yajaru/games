;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages streets-of-rage)
  #:use-module (nonguix utils)
  #:use-module (nonguix licenses)
  #:use-module (guix packages)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages game-development)
  #:use-module (nongnu packages compression))

(define-public streets-of-rage-remake
  (package
    (name "streets-of-rage-remake")
    (version "5.1")
    (source (origin
              (method url-fetch/tarbomb)
              (uri (string-append "http://launchpad.net/~ubuntugames/"
                                  "+archive/ubuntu/games/+files/sorr_"
                                  version ".orig.tar.gz"))
              (sha256
               (base32
                "1gld4i6ma265j7jw06c1drfp7pbg3n8dpm4lg9xgg28dkl5flwai"))))
    (build-system copy-build-system)
    (inputs
     `(("bash" ,bash-minimal)
       ("bennu-game-development" ,bennu-game-development)
       ("bennu-game-development-modules" ,bennu-game-development-modules)))
    (arguments
     `(#:system "i686-linux"
       #:install-plan
       '(("." "./share/sorr/"
          ;; Prefix with "./" to avoid matching files with same name in subdirectories.
          #:exclude ("./sorr" "./sorr.desktop" "./sorr.png")
          #:exclude-regexp ("bgdi" "bennugd" "data"))
         ("sorr.desktop" "./share/applications/")
         ("sorr.png" "./share/pixmaps/"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'install-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bennugd (assoc-ref inputs "bennu-game-development"))
                    (bgdi (string-append bennugd "/bin/bgdi"))
                    (bennugd-modules (assoc-ref inputs "bennu-game-development-modules"))
                    (sorr (string-append out "/bin/sorr"))
                    (icon (string-append out "/share/pixmaps/sorr.png"))
                    (desktop (string-append out "/share/applications/sorr.desktop"))
                    (sorr-path (string-append out "/share/sorr/"))
                    (sorr-data (string-append sorr-path "SorR.dat")))
               ;; Remove the unneeded executable bit.
               (for-each (lambda (file)
                           (chmod file #o644))
                         (find-files sorr-path ".*"))
               (mkdir-p (dirname sorr))
               (call-with-output-file sorr
                 (lambda (p)
                   ;; Passing '-a "$0"' to exec breaks the game.
                   (format p "\
#!~a
export LD_LIBRARY_PATH=~a/lib${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH
SORR_PATH=\"${SORR_PATH:-$HOME/.config/sorr}\"
mkdir -p \"$SORR_PATH\"
cd \"$SORR_PATH\"
for i in \"~a\"/*; do
  if [ \"$(basename \"$i\")\" != \"mod\" ]; then
    ln -sf \"$i\"
  fi
done
mkdir -p mod/games
for i in \"~a\"/mod/*; do
  ln -sf \"$i\" mod/
done
exec ~a ~a \"$@\"~%"
                           (which "sh")
                           bennugd-modules
                           sorr-path sorr-path
                           bgdi
                           sorr-data)))
               (chmod sorr #o755)
               (substitute* desktop
                 (("Icon=sorr.png")
                  (string-append "Icon=" icon))))
             #t)))))
    (home-page "https://sorr.forumotion.net/")
    (synopsis "Remake of the classic Streets of Rage 1, 2 and 3")
    (description "This is a Bombergames remake of the classic Streets of Rage 1,
2 and 3 by Sega.  The save games are stored in ~/.config/sorr (configurable via
the SORR_PATH environment variable).  Mods can be installed in
$SORR_PATH/mod/games.

Note: The game seems to only work with a 32-bit version of Bennu Game
Development.")
    (supported-systems '("i686-linux" "x86_64-linux"))
    (license (nonfree "No URL"))))

;; We keep 5.1 since 5.2 is not guaranteed to be compatible with savegames and mods.
(define-public streets-of-rage-remake-5.2
  (package
    (inherit streets-of-rage-remake)
    (version "5.2")
    (source #f)                         ; See description for why there is no source.
    (native-inputs
     `(("unrar" ,unrar)))
    (arguments
     `(#:system "i686-linux"
       #:install-plan
       '(("." "./share/sorr/"
          ;; Prefix with "./" to avoid matching files with same name in subdirectories.
          #:exclude-regexp ("bgdi" "bennugd" "data" "xbox" "\\.dll$" "\\.exe")))
       #:phases
       (modify-phases %standard-phases
         (replace 'unpack
           (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "unrar")
                       "x"
                       (assoc-ref inputs "source"))
               (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
               #t))
         (add-after 'install 'install-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bennugd (assoc-ref inputs "bennu-game-development"))
                    (bgdi (string-append bennugd "/bin/bgdi"))
                    (bennugd-modules (assoc-ref inputs "bennu-game-development-modules"))
                    (sorr (string-append out "/bin/sorr"))
                    (comment "A remake of the classic Streets of Rage 1, 2 and 3 by Sega")
                    (desktop (string-append out "/share/applications/sorr.desktop"))
                    (sorr-path (string-append out "/share/sorr/"))
                    (sorr-data (string-append sorr-path "SorR.dat")))
               ;; Remove the unneeded executable bit.
               (for-each (lambda (file)
                           (chmod file #o644))
                         (find-files sorr-path ".*"))
               (mkdir-p (dirname sorr))
               (call-with-output-file sorr
                 (lambda (p)
                   ;; Passing '-a "$0"' to exec breaks the game.
                   (format p "\
#!~a
export LD_LIBRARY_PATH=~a/lib${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH
SORR_PATH=\"${SORR_PATH:-$HOME/.config/sorr}\"
mkdir -p \"$SORR_PATH\"
cd \"$SORR_PATH\"
for i in \"~a\"/*; do
  if [ \"$(basename \"$i\")\" != \"mod\" ]; then
    ln -sf \"$i\"
  fi
done
mkdir -p mod/games
for i in \"~a\"/mod/*; do
  ln -sf \"$i\" mod/
done
exec ~a ~a \"$@\"~%"
                           (which "sh")
                           bennugd-modules
                           sorr-path sorr-path
                           bgdi
                           sorr-data)))
               (chmod sorr #o755)
               (make-desktop-entry-file desktop
                                        #:name "Streets of Rage Remake"
                                        #:comment comment
                                        #:exec sorr
                                        #:categories '("Application" "Game")))
             #t)))))
    (home-page "https://sorr.forumotion.net/")
    (description "This is a Bombergames remake of the classic Streets of Rage 1,
2 and 3 by Sega.  The save games are stored in ~/.config/sorr (configurable via
the SORR_PATH environment variable).  Mods can be installed in
$SORR_PATH/mod/games.

Note: The game seems to only work with a 32-bit version of Bennu Game
Development.

This version does not have a stable download link (for now, if some day it
does, please report!), so you need to install provide the source manually,
e.g. with @code{--with-source=streets-of-rage-remake=/PATH/TO/RAR-ARCHIVE}.

For now, check out the homepage for download links.")))
