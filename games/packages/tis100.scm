;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages tis100)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define-public gog-tis100
  (let* ((buildno "16765")
         (arch (match (or (%current-target-system)
                          (%current-system))
                 ("x86_64-linux" "x86_64")
                 ("i686-linux" "x86")
                 (_ "")))
         (binary (string-append "tis100." arch))
         (screen-selector (string-append "tis100_Data/Plugins/" arch
                                         "/ScreenSelector.so")))
    (package
      (name "gog-tis100")
      (version "11.27.2017")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://tis100/en3installer0")
         (file-name (string-append "tis_100_en_"
                                   (string-replace-substring version "." "_")
                                   "_" buildno ".sh"))
         (sha256
          (base32
           "16bc9lq45w6mpwzmllll952rznidd9hyasa968257lrn67dmx8j3"))))
      (supported-systems '("i686-linux" "x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `((,,binary
            ("libc" "eudev" "gcc:lib" "libstdc++" "libx11" "libxcursor"
             "libxrandr" "mesa" "pulseaudio" "zlib"))
           (,,screen-selector
            ("libc" "gcc:lib" "gdk-pixbuf" "glib" "gtk+" "libstdc++")))))
      (inputs
       `(("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("glib" ,glib)
         ("gtk+" ,gtk+-2)
         ("libstdc++" ,(make-libstdc++ gcc))
         ("libx11" ,libx11)
         ("libxcursor" ,libxcursor)
         ("libxrandr" ,libxrandr)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)
         ("zlib" ,zlib)))
      (home-page "http://www.zachtronics.com/tis-100/")
      (synopsis "Assembly language puzzle game that nobody asked for")
      (description
       "TIS-100 is an open-ended programming game in which you rewrite
corrupted code segments to repair the TIS-100 and unlock its secrets.  It's the
assembly language programming game you never asked for!

@itemize
@item
Print and explore the TIS-100 manual, which details the inner-workings of the
TIS-100 while evoking the aesthetics of a vintage computer manual!
@item
Solve more than 20 puzzles, competing against your friends and the world to
minimize your cycle, instruction, and node counts.
@item
Design your own challenges in the TIS-100's 3 sandboxes, including a \"visual
console\" that lets you create your own games within the game!
@item
Uncover the mysteries of the TIS-100@dots{} who created it, and for what
purpose?
@end itemize")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
